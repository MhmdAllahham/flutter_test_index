import 'package:flutter/material.dart';
import 'package:flutter_test_index/screens/introduction_screen/introduction_screen.dart';
import 'package:flutter_test_index/theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Test Index',
      theme: themeData,
      debugShowCheckedModeBanner: false,
      home: const IntroductionScreen(),
    );
  }
}
