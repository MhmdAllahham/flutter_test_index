import 'package:flutter/material.dart';
import 'package:flutter_test_index/theme.dart';

class RegisterButton extends StatelessWidget {
  final TextStyle? style;

  const RegisterButton({super.key, this.style});

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return GestureDetector(
      onTap: () {
        //TODO -- route to registerScreen
      },
      child: Container(
        width: double.maxFinite,
        padding: const EdgeInsets.symmetric(vertical: 12.0),
        decoration: BoxDecoration(
          gradient: gradientColor2,
          borderRadius: const BorderRadius.all(
            Radius.circular(borderRadius),
          ),
        ),
        child: Text(
          'Kostenlos Registrieren',
          style: style ??
              textTheme.labelMedium!.copyWith(color: gradientBackgroundColor1),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
