import 'package:flutter/material.dart';
import 'package:flutter_test_index/theme.dart';

class TapsWidget extends StatefulWidget {
  const TapsWidget({super.key});

  @override
  State<TapsWidget> createState() => _TapsWidgetState();
}

class _TapsWidgetState extends State<TapsWidget> {
  int index = 0;
  final List<String> tapLabels = [
    'Arbeitnehmer',
    'Arbeitgeber',
    'Temporärbüro',
  ];

  @override
  Widget build(BuildContext context) {
    final textStyle = Theme.of(context).textTheme;
    const padding =
        EdgeInsets.only(top: 12.0, bottom: 11.0, left: 17.0, right: 17.0);
    const width = 160.0;
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 27.0, horizontal: 20.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(borderRadius),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Stack(
            fit: StackFit.passthrough,
            children: [
              Row(mainAxisSize: MainAxisSize.min, children: [
                for (int i = 0; i < tapLabels.length; i++)
                  LayoutBuilder(builder: (context, constrains) {
                    return GestureDetector(
                      onTap: () {
                        setState(() {
                          index = i;
                        });
                        //TODO -- change the content
                      },
                      child: Container(
                        width: width,
                        padding: padding,
                        decoration: BoxDecoration(
                          border: Border.all(color: borderColor, width: 0.5),
                          borderRadius: _getSuitableBorderRadius(i),
                        ),
                        child: Text(
                          tapLabels[i],
                          textAlign: TextAlign.center,
                          style: textStyle.labelSmall
                              ?.copyWith(color: accentColor),
                        ),
                      ),
                    );
                  })
              ]),
              AnimatedContainer(
                width: width,
                duration: const Duration(milliseconds: 500),
                padding: padding,
                alignment: Alignment.center,
                margin: EdgeInsets.only(left: width * index),
                decoration: BoxDecoration(
                  borderRadius: _getSuitableBorderRadius(index),
                  color: accentColor,
                ),
                child: Text(
                  tapLabels[index],
                  style: textStyle.labelSmall?.copyWith(color: white),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  BorderRadius? _getSuitableBorderRadius(int tapIndex) {
    if (tapIndex == 0) {
      return const BorderRadius.only(
          bottomLeft: Radius.circular(borderRadius),
          topLeft: Radius.circular(borderRadius));
    } else if (tapIndex == tapLabels.length - 1) {
      return const BorderRadius.only(
          bottomRight: Radius.circular(borderRadius),
          topRight: Radius.circular(borderRadius));
    }
    return null;
  }
}
