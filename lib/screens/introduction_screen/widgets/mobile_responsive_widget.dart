import 'package:flutter/material.dart';
import 'package:flutter_test_index/common/custom_shapes/mobile_shape_1.dart';
import 'package:flutter_test_index/gen/assets.gen.dart';
import 'package:flutter_test_index/screens/introduction_screen/widgets/taps_widget.dart';

class MobileResponsiveWidget extends StatelessWidget {
  const MobileResponsiveWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final textStyle = Theme.of(context).textTheme;
    return SingleChildScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      child: Column(
        children: [
          Stack(
            children: [
              CustomPaint(
                size: Size(width, width * 1.7142857142857142),
                painter: MobileShape1(),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 103.0, right: 30, left: 30),
                    child: Text(
                      'Deine Job website',
                      style: textStyle.labelLarge,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Assets.undrawAgreementAajr.image(height: 404),
                ],
              ),
            ],
          ),
          const TapsWidget(),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
            child: Text(
              'Drei einfache Schritte zu deinem neuen Job',
              style: textStyle.titleMedium,
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(25.0),
            child: Column(
              children: [
                Align(
                    alignment: Alignment.topRight,
                    child: Assets.undrawProfileDataReV81r.image()),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: Row(
                    children: [
                      Text('1.', style: textStyle.labelLarge),
                      const SizedBox(width: 24.0),
                      Text('Erstellen dein Unterunternehmensprofil',
                          style: textStyle.labelMedium),
                    ],
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 37.0, vertical: 16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Assets.undrawJobOffersKw5d.image(),
                Row(
                  children: [
                    Text('2.', style: textStyle.labelLarge),
                    const SizedBox(width: 24.0),
                    Flexible(
                      child: Text(
                          'Erhalte Vermittlungs- angebot von Arbeitgeber',
                          style: textStyle.labelMedium,
                          textAlign: TextAlign.center),
                    ),
                  ],
                )
              ],
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 56.0, vertical: 16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Align(
                    alignment: Alignment.center,
                    child: Assets.undrawBusinessDealCpi9.image()),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: Row(
                    children: [
                      Text('3.', style: textStyle.labelLarge),
                      const SizedBox(width: 24.0),
                      Flexible(
                        child: Text(
                            'Vermittlung nach Provision oder Stundenlohn',
                            style: textStyle.labelMedium,
                            textAlign: TextAlign.center),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          const SizedBox(height: 100),
        ],
      ),
    );
  }
}
