import 'package:flutter/material.dart';
import 'package:flutter_test_index/gen/assets.gen.dart';
import 'package:flutter_test_index/screens/introduction_screen/widgets/register_button.dart';
import 'package:flutter_test_index/screens/introduction_screen/widgets/taps_widget.dart';
import 'package:flutter_test_index/theme.dart';

class DesktopResponsiveWidget extends StatelessWidget {
  const DesktopResponsiveWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final textStyle = Theme.of(context).textTheme;
    return SingleChildScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: BoxDecoration(gradient: gradientColor1),
            child: Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 133.0,
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: width / 5,
                      ),
                      child: SizedBox(
                        width: width / 4,
                        child: Column(
                          children: [
                            Text(
                              'Deine Job website',
                              style: textStyle.labelLarge,
                              textAlign: TextAlign.center,
                            ),
                            const SizedBox(height: 65.0),
                            RegisterButton(
                              style:
                                  textStyle.labelSmall?.copyWith(color: white),
                            )
                          ],
                        ),
                      ),
                    ),
                    CircleAvatar(
                      radius: 202,
                      backgroundColor: white,
                      child: Assets.undrawAgreementAajr.image(height: 404),
                    ),
                  ],
                ),
              ),
            ),
          ),
          const TapsWidget(),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
            child: Text(
              'Drei einfache Schritte zu deinem neuen Job',
              style: textStyle.titleMedium,
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(25.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text('1.', style: textStyle.labelLarge),
                const SizedBox(width: 24.0),
                Text('Erstellen dein Unterunternehmensprofil',
                    style: textStyle.labelMedium),
                const SizedBox(width: 60.0),
                Assets.undrawProfileDataReV81r.image(),
              ],
            ),
          ),
          Assets.arrow1.image(),
          Padding(
            padding: const EdgeInsets.all(25.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Assets.undrawJobOffersKw5d.image(),
                const SizedBox(width: 60.0),
                Text('2.', style: textStyle.labelLarge),
                const SizedBox(width: 24.0),
                Flexible(
                  child: Text('Erhalte Vermittlungs- angebot von Arbeitgeber',
                      style: textStyle.labelMedium,
                      textAlign: TextAlign.center),
                ),
              ],
            ),
          ),
          Assets.arrow2.image(),
          Padding(
            padding: const EdgeInsets.all(25.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text('3.', style: textStyle.labelLarge),
                const SizedBox(width: 24.0),
                Flexible(
                  child: Text('Vermittlung nach Provision oder Stundenlohn',
                      style: textStyle.labelMedium,
                      textAlign: TextAlign.center),
                ),
                const SizedBox(width: 60.0),
                Assets.undrawBusinessDealCpi9.image()
              ],
            ),
          ),
        ],
      ),
    );
  }
}
