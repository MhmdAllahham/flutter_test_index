import 'package:flutter/material.dart';
import 'package:flutter_test_index/common/responsive.dart';
import 'package:flutter_test_index/screens/introduction_screen/widgets/desktop_responsive_widget.dart';
import 'package:flutter_test_index/screens/introduction_screen/widgets/mobile_responsive_widget.dart';
import 'package:flutter_test_index/screens/introduction_screen/widgets/register_button.dart';
import 'package:flutter_test_index/theme.dart';

class IntroductionScreen extends StatelessWidget {
  const IntroductionScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: const SafeArea(
        child: Stack(
          children: [
            Responsive(
              breakpoints: {
                ResponsiveBreakpoints.mobile: MobileResponsiveWidget(),
                ResponsiveBreakpoints.desktop: DesktopResponsiveWidget()
              },
            ),
            _LoginRowWidget(),
          ],
        ),
      ),
      bottomSheet: isDesktop(context)
          ? null
          : Container(
        decoration: BoxDecoration(gradient: gradientColor1),
        child: Container(
                alignment: Alignment.topCenter,
                height: 100.0,
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 17.0),
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(borderRadius),
                    ),
                    color: white),
                child: const RegisterButton(),
              ),
      ),
    );
  }
}

class _LoginRowWidget extends StatelessWidget {
  const _LoginRowWidget();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70.0,
      decoration: const BoxDecoration(
          color: white,
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(borderRadius),
          )),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            height: 5.0,
            decoration: BoxDecoration(gradient: gradientColor2),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 26.0, 19.0, 22.0),
            child: TextButton(
              onPressed: () {
                //TODO -- route to loginScreen
              },
              style: TextButton.styleFrom(
                padding: EdgeInsets.zero,
                textStyle: Theme.of(context).textTheme.labelSmall,
                minimumSize: const Size(0, 0),
                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
              ),
              child: const Text('login'),
            ),
          )
        ],
      ),
    );
  }
}
