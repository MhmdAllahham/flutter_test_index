import 'package:flutter/material.dart';

// Colors
const white = Color(0xFFFFFFFF);
const fontColor1 = Color(0xFF2D3748);
const fontColor2 = Color(0xFF4A5568);
const fontColor3 = Color(0xFF718096);
const backgroundColor = Color(0xFFF7FAFC);
const gradientBackgroundColor1 = Color(0xFFE6FFFA);
const _gradientBackgroundColor2 = Color(0xFFEBF4FF);
const _gradientBackgroundColor3 = Color(0xFF3182CE);
const borderColor = Color(0xFFCBD5E0);
const primaryColor = Color(0xFF81E6D9);
const accentColor = Color(0xFF319795);

// GuardianColors
final gradientColor1 = _getLinearGradient(
    [gradientBackgroundColor1, _gradientBackgroundColor2]);
final gradientColor2 = _getLinearGradient(
    [accentColor, _gradientBackgroundColor3]);

// Border radius
const borderRadius = 12.0;

final themeData = ThemeData.from(
  colorScheme: ColorScheme.fromSeed(
    seedColor: white,
    background: backgroundColor,
    onBackground: fontColor1,
    primary: primaryColor,
    onPrimary: white,
    secondary: accentColor,
  ),
  textTheme: const TextTheme(
    labelSmall: TextStyle(fontSize: 14.0, color: accentColor),
    labelMedium: TextStyle(fontSize: 16.0, color: fontColor2),
    labelLarge: TextStyle(fontSize: 42.0, color: fontColor2),
    titleMedium: TextStyle(fontSize: 21.0, color: fontColor2),
  ),
).copyWith(
  appBarTheme: const AppBarTheme(
    backgroundColor: white,
  ),
);

LinearGradient _getLinearGradient(List<Color> colors) {
  return LinearGradient(
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
      colors: colors,
      tileMode: TileMode.clamp);
}
