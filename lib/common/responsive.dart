import 'package:flutter/material.dart';

enum ResponsiveBreakpoints {
  mobile(0),
  tablet(640),
  desktop(1080);

  final int width;

  const ResponsiveBreakpoints(this.width);
}

// bool isMobile(BuildContext context) =>
//     MediaQuery.of(context).size.width < ResponsiveBreakpoints.tablet.width;
//
// bool isTablet(BuildContext context) =>
//     MediaQuery.of(context).size.width < ResponsiveBreakpoints.desktop.width &&
//     MediaQuery.of(context).size.width >= ResponsiveBreakpoints.tablet.width;

bool isDesktop(BuildContext context) =>
    MediaQuery.of(context).size.width >= ResponsiveBreakpoints.desktop.width;

class Responsive extends StatelessWidget {
  final Map<ResponsiveBreakpoints, Widget> breakpoints;

  const Responsive({super.key, required this.breakpoints});

  Widget findBreakpoint(double maxWidth) {
    final breakpointWidth = breakpoints.keys.toList().reversed.firstWhere(
          (breakpointWidth) => breakpointWidth.width < maxWidth,
        );
    return breakpoints[breakpointWidth]!;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return findBreakpoint(constraints.maxWidth);
      },
    );
  }
}
