import 'package:flutter/material.dart';
import 'package:flutter_test_index/theme.dart';

class MobileShape1 extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paintFill0 = Paint()
      ..shader = gradientColor1.createShader(Offset.zero & size)
      ..style = PaintingStyle.fill
      ..strokeCap = StrokeCap.butt
      ..strokeJoin = StrokeJoin.miter;

    Path path_0 = Path();
    path_0.moveTo(size.width * -0.0046429, size.height * -0.0035417);
    path_0.lineTo(size.width * 1.0034429, size.height * -0.0007000);
    path_0.quadraticBezierTo(size.width * 1.0029286, size.height * 0.7316417,
        size.width * 1.0034000, size.height * 0.8461333);
    path_0.cubicTo(
        size.width * 0.8956857,
        size.height * 0.6970917,
        size.width * 0.3590857,
        size.height * 1.0537917,
        size.width * 0.0003143,
        size.height * 0.9720417);
    path_0.quadraticBezierTo(size.width * 0.0017857, size.height * 0.8221250,
        size.width * -0.0046429, size.height * -0.0035417);
    path_0.close();

    canvas.drawPath(path_0, paintFill0);

    Paint paintStroke0 = Paint()
      ..shader = gradientColor1.createShader(Offset.zero & size)
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.butt
      ..strokeJoin = StrokeJoin.miter;

    canvas.drawPath(path_0, paintStroke0);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
